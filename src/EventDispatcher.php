<?php
namespace Jitesoft\Purify\Events;

use Jitesoft\Exceptions\Logic\InvalidArgumentException;
use Jitesoft\Purify\Events\Contracts\DispatcherInterface;
use Jitesoft\Purify\Events\Contracts\EventInterface;
use Jitesoft\Purify\Events\Internal\Action;
use Jitesoft\Purify\Events\Internal\Filter;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class which wraps the Hook/Filter and Action event dispatching for the WordPress CMS.
 * @since 1.0.0
 */
class EventDispatcher implements DispatcherInterface {
    private $logger;

    /**
     * @param LoggerInterface|null $logger
     */
    public function __construct(?LoggerInterface $logger = null) {
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * Dispatch a given event.
     *
     * Depending on the type of the event the return value will be either of a Filter or Action event.
     * If a filter, the `getValue()` method (and index 0 of the `getArgument()` method of the resulting value will
     * contain the filtered value, while if the type is an action, the `getArgument()` method will return a empty
     * array.
     *
     * In case a event of an invalid type is passed, a InvalidArgumentException will be thrown.
     *
     * @param EventInterface|object $event
     * @return EventInterface
     * @since 1.0.0
     * @throws InvalidArgumentException
     */
    public function dispatch(object $event): EventInterface {
        $this->logger->debug('Received event of type {type} and name {name}.', [
            'type' => $event->getType(),
            'name' => $event->getName()
        ]);

        if ($event->getType() === EventTypes::ACTION) {
            return $this->callAction($event);
        }

        if ($event->getType() === EventTypes::FILTER) {
            return $this->callHook($event);
        }

        $this->logger->error('Event passed to the dispatcher with invalid type: {type}', ['type' => $event->getType()]);
        throw new InvalidArgumentException(
            sprintf('Event of type "%s" is not valid.', $event->getType()),
            'event',
            'dispatch',
            'EventDispatcher'
        );
    }

    /**
     * @param EventInterface $event
     * @return Action
     */
    private function callAction(EventInterface $event): Action {
        $this->logger->debug('Calling "do_action" with passed event.');
        do_action($event->getName(), ...$event->getArguments());
        return new Action($event->getName());
    }

    /**
     * @param EventInterface $event
     * @return Filter
     */
    private function callHook(EventInterface $event): Filter {
        $this->logger->debug('Calling "apply_filters" with passed event.');
        $result = apply_filters($event->getName(), ...$event->getArguments());
        return new Filter($event->getName(), $result);
    }

    /**
     * Set logger to use in the EventDispatcher.
     *
     * @param LoggerInterface $logger
     * @since 1.0.0
     */
    public function setLogger(LoggerInterface $logger): void {
        $this->logger = $logger;
    }

}
