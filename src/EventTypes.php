<?php
namespace Jitesoft\Purify\Events;

/**
 * A class providing constant and static methods for event types.
 */
final class EventTypes {
    public const ACTION = 'action';
    public const FILTER = 'filter';

    private const TYPES = ['action', 'filter'];

    /**
     * Check if the given type is an existing type.
     *
     * @param string $type
     * @return bool
     */
    public static function exists(string $type): bool {
        return in_array($type, self::TYPES, true);
    }

    private function __construct() {
        /* Private constructor to not make it possible to create or inherit class. */
    }

}
