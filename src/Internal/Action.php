<?php
namespace Jitesoft\Purify\Events\Internal;

use Jitesoft\Exceptions\Logic\InvalidOperationException;
use Jitesoft\Purify\Events\Contracts\EventInterface;
use Jitesoft\Purify\Events\EventTypes;

/**
 * Internal class used as a return argument for the Dispatcher.
 * @internal
 */
final class Action implements EventInterface {
    private $name;

    /**
     * @internal
     * @param string $name
     */
    public function __construct(string $name) {
        $this->name = $name;
    }

    /**
     * Get arguments from the event.
     *
     * @return array
     * @since 1.0.0
     */
    public function getArguments(): array {
        return [];
    }

    /**
     * Get the event name.
     *
     * @return string
     * @since 1.0.0
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Get the event type.
     *
     * @see   EventTypes for a list of valid types.
     * @return string
     * @since 1.0.0
     */
    public function getType(): string {
        return EventTypes::ACTION;
    }

    /**
     * Check if the propagation is stopped, in case it is, the event should
     * not be passed to more listeners but rather be returned to the dispatcher.
     *
     * @return bool
     * @since 1.0.0
     */
    public function isPropagationStopped(): bool {
        return false;
    }

    /**
     * Mark the event as stopped.
     *
     * @since 1.0.0
     * @throws InvalidOperationException
     */
    public function setPropagationStopped(): void {
        throw new InvalidOperationException('Can not set propagation to stopped on this event.');
    }
}
