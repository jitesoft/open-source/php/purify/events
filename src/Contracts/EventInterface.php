<?php
namespace Jitesoft\Purify\Events\Contracts;

use Jitesoft\Purify\Events\EventTypes;
use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Interface required to be implemented by event classes.
 *
 * @since 1.0.0
 */
interface EventInterface extends StoppableEventInterface {

    /**
     * Get arguments from the event.
     *
     * @return array
     * @since 1.0.0
     */
    public function getArguments(): array;

    /**
     * Get the event name.
     *
     * @return string
     * @since 1.0.0
     */
    public function getName(): string;

    /**
     * Get the event type.
     *
     * @see EventTypes for a list of valid types.
     * @return string
     * @since 1.0.0
     */
    public function getType(): string;

    /**
     * Check if the propagation is stopped, in case it is, the event should
     * not be passed to more listeners but rather be returned to the dispatcher.
     *
     * @return bool
     * @since 1.0.0
     */
    public function isPropagationStopped(): bool;

    /**
     * Mark the event as stopped.
     *
     * @since 1.0.0
     */
    public function setPropagationStopped(): void;

}
