<?php
namespace Jitesoft\Purify\Events\Contracts;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface for event listeners.
 *
 * @since 1.0.0
 */
interface EventListenerInterface extends LoggerAwareInterface {

    /**
     * Get the priority of the event listener.
     *
     * @return int
     * @since 1.0.0
     */
    public function getPriority(): int;

    /**
     * Set the priority of the event listener.
     *
     * @param int $priority
     * @since 1.0.0
     */
    public function setPriority(int $priority = 0): void;

    /**
     * Event handle method, the event that the listener is subscribed to will be passed and the same
     * (modified if wanted) should be returned.
     *
     * @param EventInterface $event
     * @return EventInterface
     * @since 1.0.0
     */
    public function handle(EventInterface $event): EventInterface;

    /**
     * Set the logger used by the EventListener.
     *
     * @param LoggerInterface $logger
     * @since 1.0.0
     */
    public function setLogger(LoggerInterface $logger);

}
