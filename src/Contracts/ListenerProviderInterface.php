<?php
namespace Jitesoft\Purify\Events\Contracts;

use Jitesoft\Purify\Events\EventTypes;
use Psr\EventDispatcher\ListenerProviderInterface as PSR;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface for event listener providers.
 *
 * @since 1.0.0
 */
interface ListenerProviderInterface extends PSR, LoggerAwareInterface {

    /**
     * Subscribe to a given event by type and name.
     * Priority can be set via passed integer but the priority set in the listener will be used first hand in case a
     * listener of EventListenerInterface type is passed.
     *
     * @see EventTypes for available event types.
     * @param string                          $type
     * @param string                          $event
     * @param EventListenerInterface|callable $listener
     * @param int                             $priority
     * @return bool
     * @since 1.0.0
     */
    public function subscribe(string $type, string $event, $listener, int $priority = 0): bool;

    /**
     * @param EventInterface|object $event
     * @return iterable|array|EventListenerInterface[]
     * @since 1.0.0
     */
    public function getListenersForEvent(object $event): iterable;

    /**
     * Set logger to use in the ListenerProvider.
     *
     * @param LoggerInterface $logger
     * @since 1.0.0
     */
    public function setLogger(LoggerInterface $logger): void;

}
