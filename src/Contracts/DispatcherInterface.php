<?php
namespace Jitesoft\Purify\Events\Contracts;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface for event dispatcher classes.
 *
 * @since 1.0.0
 */
interface DispatcherInterface extends EventDispatcherInterface, LoggerAwareInterface {

    /**
     * Dispatch a given event.
     *
     * @param EventInterface|object $event
     * @return EventInterface
     * @since 1.0.0
     */
    public function dispatch(object $event): EventInterface;

    /**
     * Set logger to use in the Dispatcher.
     *
     * @param LoggerInterface $logger
     * @since 1.0.0
     */
    public function setLogger(LoggerInterface $logger);

}
