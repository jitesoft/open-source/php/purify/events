<?php
namespace Jitesoft\Purify\Events;

use Jitesoft\Purify\Events\Contracts\EventInterface;
use Jitesoft\Purify\Events\Contracts\EventListenerInterface;
use Jitesoft\Purify\Events\Contracts\ListenerProviderInterface;
use Psr\Log\LoggerInterface;

class ListenerProvider implements ListenerProviderInterface {

    /**
     * Subscribe to a given event by type and name.
     * Priority can be set via passed integer but the priority set in the listener will be used first hand in case a
     * listener of EventListenerInterface type is passed.
     *
     * @see   EventTypes for available event types.
     * @param string                          $type
     * @param string                          $event
     * @param EventListenerInterface|callable $listener
     * @param int                             $priority
     * @return bool
     * @since 1.0.0
     */
    public function subscribe(string $type, string $event, $listener, int $priority = 0): bool {
        // TODO: Implement subscribe() method.
    }

    /**
     * @param EventInterface|object $event
     * @return iterable|array|EventListenerInterface[]
     * @since 1.0.0
     */
    public function getListenersForEvent(object $event): iterable {
        // TODO: Implement getListenersForEvent() method.
    }

    /**
     * Set logger to use in the ListenerProvider.
     *
     * @param LoggerInterface $logger
     * @since 1.0.0
     */
    public function setLogger(LoggerInterface $logger): void {
        // TODO: Implement setLogger() method.
    }

}
