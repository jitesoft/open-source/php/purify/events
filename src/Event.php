<?php
namespace Jitesoft\Purify\Events;

use Jitesoft\Purify\Events\Contracts\EventInterface;

class Event implements EventInterface {
    private $stopped;
    private $name;
    private $type;
    private $arguments;

    public function __construct(string $name, string $type, ...$arguments) {
        $this->stopped = false;

        $this->name      = $name;
        $this->type      = $type;
        $this->arguments = $arguments;
    }

    public function getArguments(): array {
        return $this->arguments;
    }

    public function updateArgument(int $index, $value) {
        $this->arguments[$index] = $value;
    }

    /**
     * Get the event name.
     *
     * @return string
     * @since 1.0.0
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Get the event type.
     *
     * @see   EventTypes for a list of valid types.
     * @return string
     * @since 1.0.0
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * Check if the propagation is stopped, in case it is, the event should
     * not be passed to more listeners but rather be returned to the dispatcher.
     *
     * @return bool
     * @since 1.0.0
     */
    public function isPropagationStopped(): bool {
        return $this->stopped;
    }

    /**
     * Mark the event as stopped.
     *
     * @since 1.0.0
     */
    public function setPropagationStopped(): void {
        $this->stopped = true;
    }

}
