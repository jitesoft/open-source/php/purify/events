<?php
namespace Jitesoft\Purify\Events\Tests;

use Jitesoft\Exceptions\Logic\InvalidArgumentException;
use Jitesoft\Purify\Events\Event;
use Jitesoft\Purify\Events\EventDispatcher;
use Jitesoft\Purify\Events\EventTypes;
use Jitesoft\Purify\Events\Internal\Action;
use Jitesoft\Purify\Events\Internal\Filter;
use phpmock\phpunit\PHPMock;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class EventDispatcherTest extends TestCase {
    use PHPMock;

    private $namespace;

    protected function setUp(): void {
        parent::setUp();

        // We need the namespace to be able to mock the global functions that are used by wordpress
        $this->namespace = (new ReflectionClass(EventDispatcher::class))->getNamespaceName();
    }

    public function testDispatchAction(): void {
        $applyFilterMock = $this->getFunctionMock($this->namespace, 'apply_filters');
        $doActionMock    = $this->getFunctionMock($this->namespace, 'do_action');

        $doActionMock->expects($this->once())->with($this->equalTo('test'));
        $applyFilterMock->expects($this->never());

        $dispatcher = new EventDispatcher();
        $result     = $dispatcher->dispatch(new Event('test', EventTypes::ACTION));

        $this->assertInstanceOf(Action::class, $result);
        $this->assertEquals([], $result->getArguments());
        $this->assertEquals('test', $result->getName());
        $this->assertEquals(EventTypes::ACTION, $result->getType());
    }

    public function testDispatchFilter(): void {
        $applyFilterMock = $this->getFunctionMock($this->namespace, 'apply_filters');
        $doActionMock    = $this->getFunctionMock($this->namespace, 'do_action');

        $applyFilterMock->expects($this->once())->with($this->equalTo('test'))->willReturn('abc123');
        $doActionMock->expects($this->never());

        $dispatcher = new EventDispatcher();
        $result     = $dispatcher->dispatch(new Event('test', EventTypes::FILTER));

        $this->assertInstanceOf(Filter::class, $result);
        $this->assertEquals('abc123', $result->getValue());
        $this->assertEquals(['abc123'], $result->getArguments());
        $this->assertEquals('test', $result->getName());
        $this->assertEquals(EventTypes::FILTER, $result->getType());
    }

    public function testDispatchInvalid(): void {
        $applyFilterMock = $this->getFunctionMock($this->namespace, 'apply_filters');
        $doActionMock    = $this->getFunctionMock($this->namespace, 'do_action');

        $applyFilterMock->expects($this->never());
        $doActionMock->expects($this->never());

        $dispatcher = new EventDispatcher();
        $exMessage  = '';
        try {
            $dispatcher->dispatch(new Event('test', 'abc'));
        } catch (InvalidArgumentException $e) {
            $exMessage = $e->getMessage();
        }

        $this->assertEquals('Event of type "abc" is not valid.', $exMessage);
    }

}
