<?php
namespace Jitesoft\Purify\Events\Tests;

use Jitesoft\Purify\Events\EventTypes;
use PHPUnit\Framework\TestCase;

class EventTypesTest extends TestCase {

    public function testActionExists(): void {
        $this->assertTrue(EventTypes::exists(EventTypes::ACTION));
    }

    public function testFilterExists(): void {
        $this->assertTrue(EventTypes::exists(EventTypes::FILTER));
    }

}
